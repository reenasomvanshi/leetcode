# find targate sum in BST

# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
"""
Input: 
    5
   / \
  3   6
 / \   \
2   4   7

Target = 9

Output: True
"""   
def findTarget(self, root, k):
    """
    :type root: TreeNode
    :type k: int
    :rtype: bool
    """
    d =set()
    stack =[root]
    if root is None:
        return

    while len(stack)>0:
        t= stack.pop()
        if k-t.val in d:
            return True

        else:
            d.add(t.val) 

            if t.left:
                stack.append(t.left)
            if t.right:
                stack.append(t.right)
    return False

