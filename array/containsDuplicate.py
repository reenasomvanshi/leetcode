#check if array contains dups -

def containsDuplicate(nums):
    """
    :type nums: List[int]
    :rtype: bool
    """
    unique = set()
    dup  =  set()

    for i in range(0,len(nums)):
        if nums[i] in unique:
            dup.add(nums[i])
        else:
            unique.add(nums[i])
    if len(dup)>0:
        return True
    else:
        return False

