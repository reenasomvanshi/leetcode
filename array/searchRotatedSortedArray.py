#search element in rotateed sorted array
def search(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: int
    """

    s=0
    e =len(nums)-1
    pivotIdx,s,e=findIndex(s,e,nums)
    return search_recur(pivotIdx,e,nums,target)


def findIndex(s,e,arr):
    m = s+e/2

    if arr[m]>arr[m+1]:
        pivotIdx =m+1
    else:
        if arr[s]>arr[m]:
            # pivot element is available in this part
            e=m-1
            findIndex(s,m-1,arr)
        else:
            s=m+1
            findIndex(m+1,e,arr)
    return pivotIdx,s,e
def search_recur(s,e,arr,target):
    m =(s+e)/2
    #print s,e,m
    if arr[s]==target:
        return s
    if arr[m] == target:
        return m
    if target <arr[m]:
        search_recur(s,m-1,arr,target)
    else:
        search_recur(m+1,e,arr,target)

# test case
print search([4,5,6,7,0,1,2],0)
