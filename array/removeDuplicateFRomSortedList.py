"""

Inplace dups removing
Important notes -

Never remove or del from the list while iterating 
While comparing to the next item from the list never do l[i]=l[i+1]-- changes are you will go out of index
you should always take first ele in some temp like below x =nums[0] and start the loop from 1 to len. and compare nums[i] with x.

"""
class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) ==0:
            return 0
        if len(nums) ==1:
            return 1
        x=nums[0]
        k=1
        
        for i in range(1,len(nums)):
            if nums[i] != x:
                x=nums[i]
                nums[k]=x
                k=k+1
        return k
