def maxProductNew(nums):

    maximum=big=small=nums[0]
    for n in nums[1:]:
        print n, n*big, n*small
        big, small=max(n, n*big, n*small), min(n, n*big, n*small)
        maximum=max(maximum, big)
    return maximum

#test case
nums =[-2,0,-1]
maxProductNew(nums)
