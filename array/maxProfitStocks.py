#Buy or sell stocks with maximum profit
def maxProfit(prices):
    """
    :type prices: List[int]
    :rtype: int
    """
    if not prices:
        return 0
    lowestBuy =prices[0]
    profit =0


    for v in prices:
        p = v-lowestBuy

        if profit < p:
            profit=p
        if v < lowestBuy:

            lowestBuy=v
    return profit

#test case
prices =[7,1,5,3,6,4]
print maxProfit(prices)

