# max sum sub array - output the array-- Kaden's algorithm

def subarrayWithMaxSum(nums):
    max_so_far = nums[0]
    max_ending_here =0
    start =0
    end =0
    s =0
    
    for i in range(0,len(nums)):
        max_ending_here= max_ending_here+nums[i]
    
        if max_so_far < max_ending_here:
            max_so_far = max_ending_here
            end=i
            start=s
        if max_ending_here < 0:
            max_ending_here=0
            s =i+1
            
    return nums[start:end]

#test case
nums=[-2,1,-3,4,-1,2,1,-5,4]
print subarrayWithMaxSum(nums)
