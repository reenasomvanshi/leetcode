class Node(object):
    def __init__(self,data=None):
        self.data =data
        self.left =None
        self.right =None
        
class BinaryTree:
    def __init__(self,root):
        node =Node(root)
        self.root =node
    def print_tree(self,traversal_type):
        if traversal_type =="preorder":
            print "Preorder :"
            self.preorder_print(self.root)
        if traversal_type =="inorder":
            print "Inorder :"
            self.inorder_print(self.root)
        if traversal_type =="postorder":
            print "Postorder :"
            self.postorder_print(self.root)
    def preorder_print(self,node):
        if node is not None:
            print node.data
            self.preorder_print(node.left)
            self.preorder_print(node.right)
            
    def inorder_print(self,root):
        if root is not None:

            self.preorder_print(root.left)
            print root.data
            self.preorder_print(root.right)
    def postorder_print(self,node):
        if node is not None:
            self.preorder_print(node.left)
            self.preorder_print(node.right)
            print node.data
            
            
            
            
             
        
            

        
        
            
#set up the tree
tree2 =BinaryTree(1)
tree2.root.left =Node(2)
tree2.root.right =Node(3)
tree2.root.left.left=Node(4)
tree2.root.left.right = Node(5)
tree2.root.right.left=Node(6)
tree2.root.right.right=Node(7)

#print tree2.print_tree("preorder")
print tree2.print_tree("inorder")
#print tree2.print_tree("postorder")

